import com.example.grpc.hello.Hello;
import com.example.grpc.hello.HelloServiceGrpc;
import com.linecorp.armeria.client.Clients;

public class HelloClient {

    public static void main(String[] args) {
//THIS IS OUR CLIENT, THIS IS HOW THE USER CONNECTS TO THE SERVER
        HelloServiceGrpc.HelloServiceBlockingStub helloService = Clients.newClient(
                "gproto+http://127.0.0.1:9090/",
                HelloServiceGrpc.HelloServiceBlockingStub.class);

//        HERE WE MAKE A REQUEST TO ASK FOR A SPECIFIC NAME
        Hello.HelloRequest request = Hello.HelloRequest.newBuilder().setName("KOOS").build();
        Hello.HelloReply reply = helloService.hello(request);

        System.out.println(reply.getMessage());
    }
}
