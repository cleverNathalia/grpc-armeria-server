package hello;

import com.example.grpc.hello.Hello;
import com.example.grpc.hello.HelloServiceGrpc;
import io.grpc.stub.StreamObserver;

public class HelloService extends HelloServiceGrpc.HelloServiceImplBase {

    public void hello(Hello.HelloRequest req, StreamObserver<Hello.HelloReply> responseObserver){

        System.out.println("Inside Login");
        Hello.HelloReply.Builder response = Hello.HelloReply.newBuilder();
        if (req.getName().equals("Sushi")){
            response.setMessage("ありがと - Thank you!");
        }else {
            response.setMessage("何、それは間違っている - What? That's wrong.");
        }
        responseObserver.onNext(response.build());
        responseObserver.onCompleted();

    }
}
